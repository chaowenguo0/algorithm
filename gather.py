import asyncio

async def f():
    return 1

async def g():
    return 2

assert asyncio.get_event_loop().run_until_complete(asyncio.gather(f(), g())) == [1,2]
